from flask import Flask, render_template, request
from flask_cors import CORS, cross_origin
import csv
import json

app = Flask(__name__)
cors = CORS(app)


@app.route('/', methods=['POST', 'GET'])
def index():
    if request.method == 'POST':

        communities_total = int(request.form.get('resoloution'))

        return render_template('index.html', title='CSE6242', communities_total=communities_total, comm_array=creat_communities(communities_total), resId=get_resoloution_id(communities_total))
    else:
        return render_template('index.html', title='CSE6242', communities_total=3, comm_array=creat_communities(3), resId=1)



@app.route('/home')
def home(name, value):
    return render_template('index.html', title='CSE6242', name=name, value=value)


@app.route('/about')
def about():
    return render_template('about.html', title='CSE6242')


@app.route('/contact')
def contact():
    return render_template('contact.html', title='CSE6242')


@app.route('/getGraphNodes')
@cross_origin()
def getGraphNodes():
    return csv_to_json("static/dataset/dataset.csv")


@app.route('/getCommunities')
@cross_origin()
def getCommunities():
    return csv_to_json("static/dataset/communitiesImproved.csv")


def csv_to_json(csvFilePath):
    jsonArray = []

    # read csv file
    with open(csvFilePath, encoding='utf-8') as csvf:
        # load csv file data using csv library's dictionary reader
        csvReader = csv.DictReader(csvf)

        # convert each csv row into python dict
        for row in csvReader:
            # add this python dict to json array
            jsonArray.append(row)

    return json.dumps(jsonArray)
    # #convert python jsonArray to JSON String and write to file
    # with open(jsonFilePath, 'w', encoding='utf-8') as jsonf:
    #     jsonString = json.dumps(jsonArray, indent=4)
    #     jsonf.write(jsonString)


def creat_communities(communities_size):
    communities = []
    for index, x in enumerate(range(communities_size)):
        communities.append(index + 1)

    return communities

def get_resoloution_id(communities_size):

    if communities_size == 3:
        return 1
    elif communities_size == 6:
        return 2
    else:
        return 3



if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
