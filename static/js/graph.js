var data;
var communities;
var colors;

function graphChart(data, communities_bk, resoloutionId) {

    colors = ["#EBE645", "#89B5AF", "#FF9999", "#98BAE7", "#396EB0", "#B4C6A6", "#544179", "#E2C2B9"]

    links = data;
    nodes = {};
    communities = communities_bk;


    links.forEach(function (link) {
        link.source = nodes[link.source] || (nodes[link.source] = {name: link.source});
        link.target = nodes[link.target] || (nodes[link.target] = {name: link.target});
        link.value = Math.floor(link.value);
    });

    links_bk = links;
    update(links, resoloutionId)

}

function update(links, resoloutionId) {
    var width = 700,
        height = 600;

    d3.select("svg").remove();

    var force = d3.forceSimulation()
        .nodes(d3.values(nodes))
        .force("link", d3.forceLink(links).distance(200))
        .force('center', d3.forceCenter(width / 2, height / 2))
        .force("collide", d3.forceCollide())
        .force("x", d3.forceX())
        .force("y", d3.forceY())
        .force("charge", d3.forceManyBody().strength(-500))
        .alphaTarget(1)
        .on("tick", tick);

    var svg = d3.select("#graphChart").append("svg")
        .attr("width", width)
        .attr("height", height);


    // add the links
    var path = svg.append("g")
        .selectAll("path")
        .data(links)
        .enter()
        .append("path")
        .attr("class", function (d) {
            return "link " + d.type;
        })
        .style("stroke", "#C8E3D4")
        // .style("stroke", function (d) {
        //     console.log(d.value)
        //     if (d.value <325) return "#f0f0f0";
        //     if (d.value <950) return "#bdbdbd";
        //     if (d.value >950) return "#636363";
        //
        // })
        // .style("stroke-width", function (d) {
        //     console.log(d.value)
        //     if (d.value <325) return 1;
        //     if (d.value <950) return 2;
        //     if (d.value >950) return 3;
        //
        // });
        .style("stroke-width", 1)

    // define the nodes
    var node = svg.selectAll(".node")
        .data(force.nodes())
        .enter().append("g")
        .attr("class", "node")
        .call(d3.drag()
            .on("start", dragstarted)
            .on("drag", dragged)
            .on("end", dragended))
        .on('dblclick', releasenode)
        .on("click", clicked);


    // add the nodes
    node.append("circle")
        .attr("id", function (d) {
            return d.name;
        })
        // .attr("r", function (d) {
        //     d.weight = links.filter(function (l) {
        //         return l.source.index == d.index || l.target.index == d.index
        //     }).length;
        //     console.log(d.weight)
        //     var minRadius = 0;
        //     return minRadius + (d.weight);
        // })
        .attr("r", function (d) {
            k = links.filter(function (l) {
                return l.source.index == d.index || l.target.index == d.index
            });
            var minRadius = 10;
            return minRadius + links[d.index].value / 150;
        })

        .style("font-size", "26px")
        .style("fill", function (d) {

            item = communities.filter(function (l) {
                return l.node == d.name
            })

            if (resoloutionId == 1) return colors[item[0].groupA];
            if (resoloutionId == 2) return colors[item[0].groupB];
            if (resoloutionId == 3) return colors[item[0].groupC];
        });


    node.append("text")
        .text(function (d) {
            return d.name;
        })
        .style("stroke", "#000")
        .style("font-size", "15px")
        // .attr("transform", "translate(0, -20)")
        .attr("text-anchor", "start")
        .attr("font-weight", "bold");


    function clicked(event, d) {
        // console.log(links[d])
        // window.location.replace("http://localhost:5000/home?name=" + links[d].source.name + "&value=" + links[d].value);

    }

    // add the curvy lines
    function tick() {
        path.attr("d", function (d) {
            var dx = d.target.x - d.source.x,
                dy = d.target.y - d.source.y,
                dr = Math.sqrt(dx * dx + dy * dy);
            return "M" +
                d.source.x + "," +
                d.source.y + "A" +
                dr + "," + dr + " 0 0,1 " +
                d.target.x + "," +
                d.target.y;
        });

        node.attr("transform", function (d) {
            return "translate(" + d.x + "," + d.y + ")";
        });
    };

    function dragstarted(d) {
        if (!d3.event.active) force.alphaTarget(0.3).restart();
        d.fx = d.x;
        d.fy = d.y;
        d3.select(this) // `this` is the node where drag happend
            .select("circle")
        // .style("fill", "#fff7bc");
    };

    function dragged(d) {
        d.fx = d3.event.x;
        d.fy = d3.event.y;
    };

    function dragended(d) {
        if (!d3.event.active) force.alphaTarget(0);
        if (d.fixed == true) {
            d.fx = d.x;
            d.fy = d.y;
        } else {
            // d.fx = null;
            // d.fy = null;
        }
    };

    function releasenode(d) {
        d.fx = null;
        d.fy = null;
        d3.select(this) // `this` is the node where drag happend
            .select("circle")
            .style("fill", function (d) {
                d.length = links.filter(function (l) {
                    return l.source.index == d.index || l.target.index == d.index
                }).length;
                return colorScale(d.length)
            });
    }
}


//	filter function
function filter(filter_value, data, communities, communities_total) {


    var resId = 0;

    if (communities_total == 3) resId = 1;
    if (communities_total == 6) resId = 2;
    if (communities_total == 9) resId = 3;

    filter_communities_nodes = []
    filter_communities = communities.filter(function (l) {
        if (communities_total == 3) return l.groupA == filter_value;
        if (communities_total == 6) return l.groupB == filter_value;
        if (communities_total == 9) return l.groupC == filter_value;
        // return l.groupA == filter_value
    })

    filter_communities.forEach(function (n) {
        filter_communities_nodes.push(n.node)
    })

    data_x = data.filter(function (k) {
        return filter_communities_nodes.includes(k.source) && filter_communities_nodes.includes(k.target);
    })

    graphChart(data_x, filter_communities, resId)

};


